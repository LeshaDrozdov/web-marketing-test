//menu

$(".mobile_tap").hide();

$("#burg").on("click", function() {
  $(".mobile_tap").slideToggle();
});

//filter open animation
$(".axle").hide();
$(".manufacturer").hide();

$(".filter_title").on("click", function() {
  $(".axle").slideToggle();
  $(".manufacturer").slideToggle();
});

//filter

$(document).on("click", 'input[type="checkbox"]', function() {
  var cards = $(document).find(".card");
  var checkboxes = $(document).find('input[type="checkbox"]:checked');
  var types = {};

  cards.hide();

  checkboxes.each(function() {
    var type = $(this).data("type");

    types[type] = type;
  });

  if (Object.keys(types).length === 1) {
    Object.keys(types).map(function(key) {
      checkboxes.each(function() {
        $(".items > div[data-" + key + "=" + this.id + "]").show();
      });
    });
  } else if (Object.keys(types).length === 2) {
    var classes = [];
    var totalClasses = [];

    checkboxes.each(function() {
      var type = $(this).data("type");

      classes.push([type, this.id]);
    });

    classes.map(function(type) {
      if (type[0] === "axle_configuration") {
        classes.map(function(make) {
          if (make[0] === "make") {
            totalClasses.push("." + type[1] + "." + make[1]);
          }
        });
      }
    });

    cards.hide();

    totalClasses.map(function(totalClass) {
      $(totalClass).show();
    });
  } else {
    $(".items > div").show();
  }
});
